import colors from "vuetify/es5/util/colors";

export default {
  mode: "universal",
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: (pageTitle) => {
      return !!pageTitle ? `${pageTitle} - Техноком` : "Техноком";
    },

    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || "",
      },
    ],
    link: [{ rel: "icon", type: "image/png", href: "favicon.ico" }],
  },
  /*
   ** Customize the progress-bar color
   */
  // loading: '~/components/UI/loader.vue',
  /*
   ** Global CSS
   */
  css: [/*"~assets/styles/main.scss", */ "~assets/styles/main.css"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/components",
    "~/plugins/vue-async-computed",
    "~/plugins/vue-observe-visibility",
    "~/plugins/global_mixin",
    "~/plugins/axios-options",
    "~/plugins/siriusoauth",
    { src: "@/plugins/editor", ssr: false },
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    //'@nuxtjs/eslint-module',
    "@nuxtjs/vuetify",
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    //"@nuxtjs/auth",
    "@nuxtjs/apollo",
  ],
  /*
   ** Переменные, передающиеся в process
   * */
  env: {
    baseURL: process.env.API_URL || "https://tehnokom.su/api/v1.1/",
  },

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    // https://axios.nuxtjs.org/options.html
    baseURL: process.env.API_URL || "https://tehnokom.su/api/v1.1/",
    retry: { retries: 3 },
    credentials: true,
    // debug: true
  },
  auth: {
    // https://auth.nuxtjs.org/options.html
    redirect: false,
    token: {
      prefix: "_token.",
    },
    /* localStorage: {
      prefix: 'auth.'
    }, */
    cookie: false,
    strategies: {
      local: {
        _scheme: "~/modules/auth-sheme.js",
      },
    },
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    //customVariables: ["~/assets/variables.scss"],
    theme: {
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
        light: {
          primary: "#3f51b5",
          secondary: "#348ac7",
          clean: "#c7361e",
          accent: "#8c9eff",
          error: "#b71c1c",
        },
      },
    },
  },
  /*
   ** Apollo
   * */
  apollo: {
    clientConfigs: {
      default: "~/plugins/apollo-client-options.js",
    },
    includeNodeModules: true,
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    /*
     * * Other options
     * */
    babel: {
      presets: [
        [
          require.resolve("@nuxt/babel-preset-app"),
          {
            corejs: 3,
          },
        ],
      ],
    },
  },
};
