export default {
  namespaced: true,
  state() {
    return {
      leftDrawer: true,
      rightDrawer: false,
      currentTab: 0,
      tabs: [
        {
          id: 0,
          name: "Реальный",
          component: {
            template: "<div>Реальный</div>",
          },
          backgroundTabColor: "rgba(102, 153, 255, 0.00)",
          imageFull: "/icons/tab2_1.png",
          imageMin: "/icons/tab1b.png",
          imageMinMezo: "/icons/tab4_2.png",
          imageMinHiRes: "/icons/tab1b.png",
          wideFull: 205,
          wideMin: 48,
          min: false,
          imageStyle: 0,
        },
        {
          id: 1,
          name: "Конкордо",
          component: {
            template: "<div>Конкордо</div>",
          },
          backgroundTabColor: "rgba(85, 0, 17, 0.00)",
          imageFull: "/icons/tab1_1.png",
          imageMin: "/icons/tab2b.png",
          imageMinMezo: "/icons/tab4_1.png",
          imageMinHiRes: "/icons/tab2b.png",
          wideFull: 205,
          wideMin: 48,
          min: true,
          imageStyle: 0,
        },
        {
          id: 2,
          name: "Триумфо",
          component: {
            template: "<div>Триумфо</div>",
          },
          backgroundTabColor: "rgba(0, 104, 104, 0.00)",
          imageFull: "/icons/tab3_1.png",
          imageMin: "/icons/tab3b.png",
          imageMinMezo: "/icons/tab4_3.png",
          imageMinHiRes: "/icons/tab3b.png",
          wideFull: 205,
          wideMin: 48,
          min: true,
          imageStyle: 0,
        },
      ],
    };
  },
  getters: {
    imageStyle: (state) => (i, width) => {
      return width > 970
        ? `background-image: url( ${
            !state.tabs[i].min || width > 1264
              ? state.tabs[i].imageFull
              : state.tabs[i].imageMin
          }); width: ${
            !state.tabs[i].min || width > 1264
              ? state.tabs[i].wideFull
              : state.tabs[i].wideMin
          }px;  ${
            !state.tabs[i].min || width > 1264
              ? ""
              : "background-size: 48px 48px;"
          } `
        : `background-image: url( ${state.tabs[i].imageMin}); width: ${state.tabs[i].wideMin}px;     background-size: 48px 48px;`;
    },
    leftDrawer: (state) => (width) => {
      return (width < 1264) & (width > 600)
        ? state.leftDrawer
        : width >= 1264
        ? true
        : false;
    },
    rightDrawer: (state) => (width) => {
      return width >= 970 ? false : state.rightDrawer;
    },
    backgroundColor: (state) => {
      return state.tabs[state.currentTab].backgroundTabColor;
    },
  },
  mutations: {
    clickOnTab(state, tab) {
      if (state.currentTab === tab) {
        state.tabs[tab].min = !state.tabs[tab].min;
      } else {
        state.currentTab = tab;
        state.tabs[tab].min = false;

        for (let i = 0; i < state.tabs.length; i++) {
          i !== state.currentTab ? (state.tabs[i].min = true) : "";
        }
      }
    },
    leftDrawer(state, width) {
      (width < 1264) & (width > 600)
        ? (state.leftDrawer = !state.leftDrawer)
        : "";
    },
    rightDrawer(state, width) {
      state.rightDrawer = !state.rightDrawer;
    },
  },
  computed: {},
};
